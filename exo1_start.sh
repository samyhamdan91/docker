#!/bin/bash

docker run --name redis1 -d redis:7.0.7 
docker run --name redis2 -d redis:6.2.8

docker run --name mysql1 -d -e MYSQL_ROOT_PASSWORD=admin mysql:8.0.3
docker run --name mysql2 -d -e MYSQL_ROOT_PASSWORD=admin mysql:5.7.40
